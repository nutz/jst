# JST进化史

## 1.0.3

fix: 修正与nutzboot 2.2.1相关的bug

## 1.0.2

add: 添加对nutzboot的直接支持

## 1.0.1

add: 支持多行js片段
add: 支持html特殊字符的抹除

## 1.0.1

add: 初始化版本