package org.nutz.jst.block;

/**
 * 单纯代表一段文本,不做其他事
 * 
 * @author wendal
 *
 */
public final class JstStaticBlock extends JstBlock {

    public JstStaticBlock(String str, int lineNumber) {
        this.str = str;
        this.lineNumber = lineNumber;
    }

    public void asJs(StringBuilder sb) {}
}