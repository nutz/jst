package org.nutz.jst.block;

public abstract class JstBlock {
    public String str;
    public int lineNumber;

    public abstract void asJs(StringBuilder sb);
}