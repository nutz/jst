package org.nutz.jst.block;

public class JstEvalBlock extends JstBlock {

    protected boolean htmlSafe;

    public JstEvalBlock(String str, int lineNumber) {
        if (str.startsWith("=")) {
            htmlSafe = true;
            str = str.substring(1);
        }
        this.str = str;
        this.lineNumber = lineNumber;
    }

    public void asJs(StringBuilder sb) {
        sb.append("$out.write(''+(");
        if (htmlSafe)
            sb.append("org.nutz.lang.Strings.escapeHtml(''+(");
        sb.append(str);
        if (htmlSafe)
            sb.append("))");
        sb.append("));");
    }
}