package org.nutz.jst.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.jst.impl.JstImpl;
import org.nutz.jst.loader.JstLoader;
import org.nutz.lang.util.Context;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.view.AbstractPathView;

public class JstView extends AbstractPathView {

    private static final Log log = Logs.get();

    protected JstLoader loader;

    public JstView(String key, JstLoader loader) {
        super(key);
        this.loader = loader;
    }

    public void render(HttpServletRequest req, HttpServletResponse resp, Object obj) throws Throwable {
        String path = evalPath(req, obj);
        JstImpl jst = loader.get(path);
        if (jst == null) {
            log.warnf("no such template path=%s loader=%s", path, loader.forPrint());
            resp.sendError(500);
            return;
        }
        Context ctx = createContext(req, obj);
        jst.render(ctx, resp.getWriter());
    }

}
