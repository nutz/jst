package org.nutz.jst.mvc;

import java.io.InputStream;

import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.jst.loader.JstLoader;
import org.nutz.jst.loader.JstLoaderFactory;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.View;
import org.nutz.mvc.ViewMaker;

public class JstViewMaker implements ViewMaker {

    private static final Log log = Logs.get();

    protected JstLoader loader;
    
    public JstViewMaker() {
    }

    public JstViewMaker(JstLoader loader) {
        this.loader = loader;
    }

    public View make(Ioc ioc, String type, String value) {
        if (!"jst".equals(type))
            return null;
        if (loader == null) {
            if (ioc.has("jstLoader")) {
                loader = ioc.get(JstLoader.class, "jstLoader");
            } else {
                PropertiesProxy conf;
                InputStream ins = getClass().getClassLoader().getResourceAsStream("jst.properties");
                if (ins != null) {
                    log.info("load jst configure from jst.properties");
                    conf = new PropertiesProxy(ins);
                } else if (ioc.has("conf")) {
                    log.info("load jst configure from IocName=conf");
                    conf = ioc.get(PropertiesProxy.class, "conf");
                } else {
                    log.info("load jst default configure");
                    conf = new PropertiesProxy();
                }
                loader = JstLoaderFactory.make(ioc, conf);
            }
        }
        return new JstView(value, loader);
    }
}
