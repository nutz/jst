package org.nutz.jst;

@SuppressWarnings("serial")
public class JstException extends RuntimeException {

    public JstException() {
        super();
    }

    public JstException(String message, Throwable cause) {
        super(message, cause);
    }

    public JstException(String message) {
        super(message);
    }

    public JstException(Throwable cause) {
        super(cause);
    }

}
