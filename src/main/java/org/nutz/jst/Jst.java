package org.nutz.jst;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.nutz.jst.impl.JstImpl;
import org.nutz.lang.Files;
import org.nutz.lang.util.Context;

/**
 * 基于JDK8+的nashorn js引擎的模板引擎
 * 
 * @author wendal(wendal1985@gmail.com)
 *
 */
public class Jst {

    /**
     * 默认引擎
     */
    protected static ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");
    protected static Map<String, Object> G = new HashMap<String, Object>();

    /**
     * 设置全局引擎
     * @param engine
     */
    public static void setGlobalEngine(ScriptEngine engine) {
        Jst.engine = engine;
    }

    /**
     * 获取全局引擎
     * @return
     */
    public static ScriptEngine getGlobalEngine() {
        return engine;
    }

    /**
     * 注册全局变量
     * @param key 不可以为null,也不可以是空字符串
     * @param value 若值是null,代表移除
     */
    public static void registerGlobal(String key, Object value) {
        if (value == null)
            G.remove(key);
        else
            G.put(key, value);
    }

    public static JstImpl create(String str) {
        return new JstImpl(str);
    }
    
    public static JstImpl createFromFile(File f) {
        return new JstImpl(Files.read(f));
    }
    
    public static String render(String tpl, Context ctx) {
        JstImpl jst = new JstImpl(tpl);
        jst.compile();
        return jst.render(ctx);
    }

    public static Map<String, Object> getGlobal() {
        return G;
    }
}
