package org.nutz.jst.loader;

import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.jst.loader.impl.JstClasspathLoader;
import org.nutz.jst.loader.impl.JstComboIocLoader;
import org.nutz.jst.loader.impl.JstLocalFileLoader;
import org.nutz.jst.loader.impl.JstWebappLoader;
import org.nutz.log.Log;
import org.nutz.log.Logs;

public class JstLoaderFactory {

    private static final Log log = Logs.get();

    public static JstLoader make(Ioc ioc, PropertiesProxy conf) {
        JstComboIocLoader combo = new JstComboIocLoader();
        for (String name : conf.get("jst.loaders", "classpath").split(",")) {
            switch (name) {
            case "classpath":
                JstClasspathLoader cp = new JstClasspathLoader();
                cp.setConf(conf, "jst.loader.classpath.");
                combo.addLoader(cp);
                break;
            case "webapp":
                JstWebappLoader webapp = new JstWebappLoader();
                webapp.setConf(conf, "jst.loader.webapp.");
                combo.addLoader(webapp);
                break;
            case "local":
                JstLocalFileLoader local = new JstLocalFileLoader();
                local.setConf(conf, "jst.loader.local.");
                combo.addLoader(local);
                break;
            default:
                log.info("not support loader type = " + name);
                break;
            }
        }
        return combo;
    }
}
