package org.nutz.jst.loader.impl;

import java.util.ArrayList;
import java.util.List;

import org.nutz.jst.impl.JstImpl;
import org.nutz.jst.loader.JstLoader;

public class JstComboIocLoader extends JstLoader {

    protected List<JstLoader> loaders = new ArrayList<JstLoader>();

    public void addLoader(JstLoader loader) {
        loaders.add(loader);
    }

    public boolean has(String key) {
        for (JstLoader loader : loaders) {
            if (loader.has(key))
                return true;
        }
        return false;
    }

    public JstImpl get(String key) {
        for (JstLoader loader : loaders) {
            if (loader.has(key))
                return loader.get(key);
        }
        return null;
    }

    public String forPrint() {
        if (loaders.size() == 1)
            return loaders.get(0).forPrint();
        StringBuilder sb = new StringBuilder("JstComboIocLoader count=" + loaders.size());
        for (JstLoader loader : loaders) {
            sb.append("\r\n  -- ").append(loader.forPrint());
        }
        return sb.toString();
    }
}
