package org.nutz.jst.loader.impl;

import java.util.HashMap;
import java.util.Map;

import org.nutz.jst.impl.JstImpl;
import org.nutz.jst.loader.JstLoader;

public class JstMemoryLoader extends JstLoader {

    protected Map<String, byte[]> datas;

    public JstMemoryLoader() {
        datas = new HashMap<>();
    }

    public JstMemoryLoader(Map<String, byte[]> datas) {
        this.datas = datas;
    }

    public boolean has(String key) {
        return datas.containsKey(key);
    }

    public JstImpl get(String key) {
        byte[] buf = datas.get(key);
        if (buf == null)
            return null;
        return build(buf);
    }

    public void put(String key, byte[] data) {
        datas.put(key, data);
    }

    public void clear() {
        datas.clear();
    }

    public String forPrint() {
        return "memory(count=" + datas.size() + ")";
    }
}
