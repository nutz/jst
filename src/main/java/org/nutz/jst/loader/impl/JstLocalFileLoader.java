package org.nutz.jst.loader.impl;

import java.io.File;

import org.nutz.jst.impl.JstImpl;
import org.nutz.jst.loader.JstLoader;
import org.nutz.lang.Files;

public class JstLocalFileLoader extends JstLoader {

    protected File dir;

    public void setRoot(String root) {
        this.dir = new File(root);
    }

    public boolean has(String key) {
        return new File(dir, key).exists();
    }

    public JstImpl get(String key) {
        File f = new File(dir, key);
        if (!f.exists()) {
            return null;
        }
        byte[] buf = Files.readBytes(f);
        return build(buf);
    }

    public String forPrint() {
        return String.format("dir(root=%s, charset=%s)", dir, charset);
    }
}
