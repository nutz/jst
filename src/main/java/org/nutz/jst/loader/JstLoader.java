package org.nutz.jst.loader;

import java.nio.charset.Charset;

import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.jst.impl.JstImpl;
import org.nutz.lang.Encoding;

public abstract class JstLoader {

    protected PropertiesProxy conf;
    protected Charset charset = Encoding.CHARSET_UTF8;
    protected String root;
    protected boolean debug;

    public abstract boolean has(String key);

    public abstract JstImpl get(String key);

    public void init() {}

    public abstract String forPrint();

    public void setConf(PropertiesProxy conf, String prefix) {
        this.conf = conf;
        setDebug(conf.getBoolean(prefix + "debug", conf.getBoolean("jst.debug", false)));
        setCharset(Charset.forName(conf.get(prefix + "charset", conf.get("jst.charset", "UTF-8"))));
        setRoot(conf.get(prefix + "root", "template"));
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    protected JstImpl build(byte[] buf) {
        String str = new String(buf, charset);
        JstImpl jst = new JstImpl(str);
        jst.setDebug(debug);
        jst.compile();
        return jst;
    }
}
