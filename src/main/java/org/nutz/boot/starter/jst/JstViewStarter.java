package org.nutz.boot.starter.jst;

import org.nutz.integration.shiro.ShiroProxy;
import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.jst.Jst;
import org.nutz.jst.loader.JstLoader;
import org.nutz.jst.loader.JstLoaderFactory;
import org.nutz.jst.mvc.JstViewMaker;

@IocBean
public class JstViewStarter {

    @Inject
    protected PropertiesProxy conf;

    @Inject("refer:$ioc")
    protected Ioc ioc;

    @IocBean(name = "jstLoader")
    public JstLoader createJstLoader() {
        return JstLoaderFactory.make(ioc, conf);
    }

    @IocBean(name = "$views_jst")
    public JstViewMaker createJstViewMaker(@Inject JstLoader jstLoader) {
        return new JstViewMaker(jstLoader);
    }
    
    public void setIoc(Ioc ioc) {
        this.ioc = ioc;
        try {
            Jst.getGlobal().put("shiro", new ShiroProxy());
        }
        catch (Throwable e) {
        }
    }
}
