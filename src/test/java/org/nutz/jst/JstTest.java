package org.nutz.jst;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nutz.jst.impl.JstImpl;
import org.nutz.lang.Lang;
import org.nutz.lang.Stopwatch;
import org.nutz.lang.Streams;
import org.nutz.lang.util.Context;
import org.nutz.lang.util.NutMap;

import junit.framework.TestCase;

public class JstTest extends TestCase {

    @Before
    public void setUp() throws Exception {
        System.setProperty("nashorn.args", "--language=es6");
    }

    @After
    public void tearDown() throws Exception {}

    /**
     * 测试最基本的功能
     */
    @Test
    public void test_very_simple() throws Exception {
        Context ctx = Lang.context();
        ctx.set("age", 30);
        ctx.set("name", "wendal");
        ctx.set("ids", new int[]{30, 40, 50, 60});

        // --------------------------------------------
        JstImpl tmpl = null;
        String str = "# if (age > 0) {\r\n${age}\r\n#}";
        tmpl = new JstImpl(str);
        tmpl.setName("test_very_simple.jst");
        System.out.println(tmpl.getBlocks().size() == 3);
        System.out.println(tmpl.compile());
        System.out.println("--------------------------");
        System.out.println(tmpl.render(ctx));
        System.out.println("==========================");
    }

    /**
     * 测试3种基本形式, 语句/常量/表达式
     */
    @Test
    public void test_simple2() throws Exception {
        Context ctx = Lang.context();
        ctx.set("age", 30);
        ctx.set("name", "wendal");
        ctx.set("ids", new int[]{30, 40, 50, 60});

        // --------------------------------------------
        JstImpl tmpl = null;

        tmpl = new JstImpl("# if (age > 0) {\r\nABC\r\n#}");
        System.out.println(tmpl.getBlocks().size() == 3);
        System.out.println(tmpl.compile());
        System.out.println("--------------------------");
        System.out.println(tmpl.render(ctx));
        System.out.println("==========================");
        tmpl = new JstImpl("# if (age > 0) {\r\n$${age}\r\n#}");
        System.out.println(tmpl.getBlocks().size() == 3);
        System.out.println(tmpl.compile());
        System.out.println("--------------------------");
        System.out.println(tmpl.render(ctx));
        System.out.println("==========================");
        tmpl = new JstImpl("# if (age > 0) {\r\n${age}\r\n#}");
        System.out.println(tmpl.getBlocks().size() == 3);
        System.out.println(tmpl.compile());
        System.out.println("--------------------------");
        System.out.println(tmpl.render(ctx));
        System.out.println("==========================");

    }

    /**
     * 跑一跑性能测试
     */
    @Test
    public void test_performance() throws Exception {
        Context ctx = Lang.context();
        ctx.set("age", 30);
        ctx.set("name", "wendal");
        ctx.set("ids", new int[]{30, 40, 50, 60});

        String str = Streams.readAndClose(Streams.utf8r(getClass().getClassLoader().getResourceAsStream("org/nutz/jst/test/tmpls/simple.jst")));
        JstImpl tmpl = new JstImpl(str);
        System.out.println(tmpl.compile());
        System.out.println("--------------------------");
        Stopwatch sw = Stopwatch.begin();
        for (int i = 0; i < 10000; i++) {
            tmpl.render(ctx);
        }
        sw.stop();
        System.out.println("预热: " + sw);
        sw = Stopwatch.begin();
        for (int i = 0; i < 10000; i++) {
            tmpl.render(ctx);
        }
        sw.stop();
        System.out.println(sw);
        System.out.println(tmpl.render(ctx));
        System.out.println("==========================");

        assertNotNull(tmpl.render(ctx));
    }

    /**
     * 能不能指出具体的报错行数呢?
     */
    @Test
    public void test_error_line() throws Exception {
        Context ctx = Lang.context();
        String path = "org/nutz/jst/test/tmpls/with_error.jst";
        String str = Streams.readAndClose(Streams.utf8r(getClass().getClassLoader().getResourceAsStream(path)));
        JstImpl tmpl = new JstImpl(str);
        tmpl.setName(path);
        tmpl.setDebug(true);
        tmpl.compile();
        try {
            tmpl.render(ctx);
            fail();
        }
        catch (JstException e) {
            e.printStackTrace();
        }
    }

    /**
     * 能不能指出具体的报错行数呢?
     */
    @Test
    public void test_error2() throws Exception {
        String path = "org/nutz/jst/test/tmpls/with_error2.jst";
        String str = Streams.readAndClose(Streams.utf8r(getClass().getClassLoader().getResourceAsStream(path)));
        JstImpl tmpl = new JstImpl(str);
        tmpl.setName(path);
        tmpl.setDebug(true);
        try {
            tmpl.compile();
            fail();
        }
        catch (JstException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_threads() throws InterruptedException {
        Context ctx = Lang.context();
        ctx.set("age", 30);
        ctx.set("name", "wendal");
        ctx.set("ids", new int[]{30, 40, 50, 60});

        String str = Streams.readAndClose(Streams.utf8r(getClass().getClassLoader().getResourceAsStream("org/nutz/jst/test/tmpls/simple.jst")));
        JstImpl tmpl = new JstImpl(str);
        tmpl.compile();
        ExecutorService es = Executors.newFixedThreadPool(16);
        Stopwatch sw = Stopwatch.begin();
        for (int i = 0; i < 100000; i++) {
            es.submit(new Runnable() {

                @Override
                public void run() {
                    try {
                        tmpl.render(ctx);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        es.shutdown();
        es.awaitTermination(1, TimeUnit.MINUTES);
        sw.stop();
        System.out.println(sw);
    }
    

    @Test
    public void test_html() throws InterruptedException {
        Context ctx = Lang.context();
        ctx.set("title", "用户列表");
        ctx.set("users", Arrays.asList(
                                       new NutMap("name", "<script>alert('hahaha')</script>wendal").setv("age", 30),
                                       new NutMap("name", "zozoh").setv("age", 40),
                                       new NutMap("name", "pangwu").setv("age", 30)
                                       ));

        String str = Streams.readAndClose(Streams.utf8r(getClass().getClassLoader().getResourceAsStream("org/nutz/jst/test/tmpls/render_jst.html")));
        JstImpl tmpl = new JstImpl(str);
        tmpl.compile();
        String re = tmpl.render(ctx);
        System.out.println(re);
        assertFalse(re.contains("<script>"));
        assertTrue(re.contains("wendal"));
        assertTrue(re.contains("zozoh"));
        assertTrue(re.contains("pangwu"));
    }
}
